package com.twassignment.database;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.net.UnknownHostException;

/**
 * establishes database connection
 */
public class DatabaseConnection {
    private static Morphia morphia;
    private static Datastore datastore;

    public static Datastore setDatabaseConnection() {
        morphia = new Morphia();
        morphia.mapPackage("/Users/barchana/biblioteca/src/com/twassignment/library");
        try {
            datastore = morphia.createDatastore(new MongoClient(), "library");
            datastore.ensureIndexes();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return datastore;
    }

}

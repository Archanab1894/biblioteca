package com.twassignment.application;

// represents parsing logic for populating books and movies objects

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.twassignment.library.*;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Parser {
    private BufferedReader br = null;
    private Morphia morphia;
    private Datastore datastore;
    private MongoClient mongoClient;
    DB db;

    private void setDatabaseConnection() {
        morphia = new Morphia();
        morphia.mapPackage("/Users/barchana/biblioteca/src/com/twassignment/library");

        try {
            mongoClient = new MongoClient();
            db = mongoClient.getDB("library");
            datastore = morphia.createDatastore(mongoClient, "library");
            datastore.ensureIndexes();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void populateBooks() {
        setDatabaseConnection();
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader("resources/ListOfBooksWithDetails"));
            DBCollection myCollection = db.getCollection("library_item");
            myCollection.drop();
            while ((sCurrentLine = br.readLine()) != null) {
                String[] parameters = sCurrentLine.split(",");
                datastore.save(createABookObject(parameters));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void populateMovies() {
        setDatabaseConnection();
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader("resources/ListOfMoviesWithDetails"));
            while ((sCurrentLine = br.readLine()) != null) {
                String[] parameters = sCurrentLine.split(",");
                datastore.save(createAMovieObject(parameters));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void populateLibrarians() {
        setDatabaseConnection();
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader("resources/LibrarianCredentials"));
            DBCollection myCollection = db.getCollection("library_user");
            myCollection.drop();
            while ((sCurrentLine = br.readLine()) != null) {
                String[] parameters = sCurrentLine.split(",");
                datastore.save(createALibrarianObject(parameters));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void populateUsers() {
        setDatabaseConnection();
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader("resources/UserCredentials"));
            DBCollection myCollection = db.getCollection("library_checkout");
            myCollection.drop();
            while ((sCurrentLine = br.readLine()) != null) {
                String[] parameters = sCurrentLine.split(",");
                datastore.save(createAUserObject(parameters));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Book createABookObject(String[] parameters) {
        return new Book(parameters[0], parameters[1], parameters[2], Integer.parseInt(parameters[3]));
    }

    private Movie createAMovieObject(String[] parameters) {
        return new Movie(parameters[0], parameters[1], parameters[2], Integer.parseInt(parameters[3]), parameters[4]);
    }

    private LibraryUser createALibrarianObject(String[] parameters) {
        return new LibraryUser(parameters[0], parameters[1], UserType.LIBRARIAN);
    }

    private LibraryUser createAUserObject(String[] parameters) {
        return new LibraryUser(parameters[0], parameters[1], UserType.CUSTOMER, parameters[2], parameters[3], parameters[4]);
    }
}


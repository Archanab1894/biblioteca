package com.twassignment.application;

// drives biblioteca application

import com.twassignment.application.Biblioteca;
import com.twassignment.iohandler.BibliotecaConsoleWriter;
import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.iohandler.BibliotecaConsoleReader;

public class BibliotecaDriver {
    public static void main(String[] args) {
        BibliotecaReadable reader = new BibliotecaConsoleReader();
        BibliotecaWriteable writer = new BibliotecaConsoleWriter();
        Biblioteca library = new Biblioteca(reader,writer);
        library.setUpLibrary();
        library.startApplication();
    }
}

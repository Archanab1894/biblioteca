package com.twassignment.application;

import com.twassignment.database.DatabaseConnection;
import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.menu.LoginMenu;
import org.mongodb.morphia.Datastore;

public class Biblioteca {

    private static final String WELCOME_MESSAGE = "Welcome to Biblioteca!!!";

    private Library library;
    private BibliotecaReadable reader;
    private BibliotecaWriteable writer;
    private Parser parser;

    private Datastore datastore;


    public Biblioteca(BibliotecaReadable reader, BibliotecaWriteable writer) {
        this.reader = reader;
        this.writer = writer;
        parser = new Parser();
    }

    public String welcomeMessage() {
        return WELCOME_MESSAGE;
    }

    public void setUpLibrary() {
        datastore = DatabaseConnection.setDatabaseConnection();
        library = new Library(datastore);
        parser.populateBooks();
        parser.populateMovies();
        parser.populateLibrarians();
        parser.populateUsers();
    }

    public void startApplication() {
        writer.display(welcomeMessage());
        new LoginMenu(library).start();
    }


}

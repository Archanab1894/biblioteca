package com.twassignment.library;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("library_checkout")
public class LibraryCheckOutsEntry {
    @Id
    ObjectId id;
    LibraryItem item;
    LibraryUser user;

    public LibraryCheckOutsEntry() {
    }

    public LibraryCheckOutsEntry(LibraryItem item, LibraryUser user) {
        this.item = item;
        this.user = user;
    }

    public boolean validateCheckOutEntry(String isbn, LibraryItemCategory category, LibraryUser currentUser, Datastore datastore) {

        if (item.isOfSameID(isbn) && item.isOfCategory(category) & user.equals(currentUser)) {
            datastore.save(item);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String result = String.format("%1$15s %2$15s", item.getName(), user.getUserNumber());
        return result;
    }
}

package com.twassignment.library;


public enum LibraryItemCategory {
    BOOK,
    MOVIE;
}

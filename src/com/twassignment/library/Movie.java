package com.twassignment.library;

import org.mongodb.morphia.annotations.Entity;

// models a entertainment unit
@Entity("library_item")
public class Movie extends LibraryItem {

    private String directorName;

    private String rating;

    public Movie() {

    }

    public Movie(String id, String name, String directorName, int year, String rating) {
        super(id, name, year, LibraryItemCategory.MOVIE);
        this.name = name;
        this.directorName = directorName;
        this.year = year;
        this.rating = rating;
    }

    @Override
    public String toString() {
        String result = String.format("%1$5s %2$20s %3$20s %4$10s %5$10s", id, name, directorName, year, rating);
        return result + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        return id != null ? id.equals(movie.id) : movie.id == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;

        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}

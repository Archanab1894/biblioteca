package com.twassignment.library;

// models repository of books and movies

import org.mongodb.morphia.Datastore;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library {

    private List<LibraryItem> listOfLibraryItems = new ArrayList<>();
    private List<LibraryCheckOutsEntry> checkedOutLibraryItems = new ArrayList<>();

    private Datastore datastore;

    public Library(Datastore datastore) {
        this.datastore = datastore;
    }


    public String getListOfItems(LibraryItemCategory category) {
        String result = "";
        listOfLibraryItems = datastore.createQuery(LibraryItem.class).asList();
        for (LibraryItem libraryItem : listOfLibraryItems) {
            if (libraryItem.isOfCategory(category))
                result += libraryItem;
        }
        return result;
    }


    public boolean checkOutItem(String id, LibraryItemCategory category, LibraryUser currentUser) {

        listOfLibraryItems = datastore.createQuery(LibraryItem.class).asList();

        Iterator<LibraryItem> iterator = listOfLibraryItems.iterator();
        while (iterator.hasNext()) {
            LibraryItem libraryItem = iterator.next();
            if (libraryItem.isOfSameID(id) && libraryItem.isOfCategory(category)) {
                LibraryCheckOutsEntry checkOutsEntry = new LibraryCheckOutsEntry(libraryItem, currentUser);
                datastore.delete(libraryItem);
                datastore.save(checkOutsEntry);
                return true;
            }
        }
        return false;
    }

    public boolean returnBackTheItem(String isbn, LibraryItemCategory category, LibraryUser currentUser) {

        checkedOutLibraryItems = datastore.createQuery(LibraryCheckOutsEntry.class).asList();
        Iterator iterator = checkedOutLibraryItems.iterator();
        while (iterator.hasNext()) {
            LibraryCheckOutsEntry checkOutsEntry = (LibraryCheckOutsEntry) iterator.next();
            if (checkOutsEntry.validateCheckOutEntry(isbn, category, currentUser,datastore)) {
                datastore.delete(checkOutsEntry);
                return true;
            }
        }
        return false;
    }

    public String viewCheckOuts() {

        checkedOutLibraryItems = datastore.createQuery(LibraryCheckOutsEntry.class).asList();
        String result = "";
        Iterator iterator = checkedOutLibraryItems.iterator();
        while (iterator.hasNext()) {
            LibraryCheckOutsEntry checkOutsEntry = (LibraryCheckOutsEntry) iterator.next();
            result += "" + checkOutsEntry + "\n";
        }
        return result;
    }
}

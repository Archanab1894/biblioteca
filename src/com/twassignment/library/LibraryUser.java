package com.twassignment.library;

import org.mongodb.morphia.annotations.Entity;

// models a user who have credentials to access the library


@Entity("library_user")
public class LibraryUser {

    private String userNumber;
    private String password;
    private UserType userType;
    private String name;
    private String emailAddress;
    private String phoneNumber;

    public LibraryUser() {
    }

    public LibraryUser(String userNumber, String password, UserType userType, String name, String phoneNumber, String emailAddress) {
        this.userNumber = userNumber;
        this.password = password;
        this.userType = userType;
        this.emailAddress = emailAddress;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public LibraryUser(String userNumber, String password, UserType userType) {
        this(userNumber, password, userType, null, null, null);
    }

    public boolean verifyCredentials(String userNumber, String password) {
        return this.userNumber.equals(userNumber) && this.password.equals(password);
    }

    public boolean isUserALibrarian() {
        return this.userType == UserType.LIBRARIAN;
    }

    public String getDetails() {
        String result = String.format("%1$10s %2$15s %3$20s", name, phoneNumber, emailAddress);
        return result;
    }

    public String getUserNumber() {
        String result = String.format("%1$10s", userNumber);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LibraryUser that = (LibraryUser) o;

        if (userNumber != null ? !userNumber.equals(that.userNumber) : that.userNumber != null) return false;
        return password != null ? password.equals(that.password) : that.password == null;

    }

    @Override
    public int hashCode() {
        int result = userNumber != null ? userNumber.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}


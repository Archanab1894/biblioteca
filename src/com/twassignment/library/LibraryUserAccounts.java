package com.twassignment.library;

// models a manager

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class LibraryUserAccounts {
    private List<LibraryUser> libraryUsers = new ArrayList<>();
    private Morphia morphia;
    private Datastore datastore;

    public LibraryUserAccounts() {
    }

    private void setDatabaseConnection() {
        morphia = new Morphia();
        morphia.mapPackage("/Users/barchana/biblioteca/src/com/twassignment/library");

        try {
            datastore = morphia.createDatastore(new MongoClient(), "library");
            datastore.ensureIndexes();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public LibraryUser fetchTheUserAccount(String userNumber, String password) {
        setDatabaseConnection();
        libraryUsers = datastore.createQuery(LibraryUser.class).asList();
        for (LibraryUser user : libraryUsers) {
            if (user.verifyCredentials(userNumber, password))
                return user;
        }
        return null;
    }
}

package com.twassignment.library;

public enum UserType {
    LIBRARIAN, CUSTOMER;
}

package com.twassignment.library;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("library_item")
public class LibraryItem {
    @Id
    protected String id;
    protected String name;
    protected int year;
    protected LibraryItemCategory category;

    public LibraryItem() {
    }

    public LibraryItem(String id, String name, int year, LibraryItemCategory category) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.category = category;
    }

    public boolean isOfSameID(String isbn) {
        return this.id.equals(isbn);
    }

    public boolean isOfCategory(LibraryItemCategory category) {
        return this.category == category;
    }

    public String getName() {
        return name;
    }
}

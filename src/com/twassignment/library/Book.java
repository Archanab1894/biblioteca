package com.twassignment.library;

// models a printed script with details

import org.mongodb.morphia.annotations.Entity;

@Entity("library_item")
public class Book extends LibraryItem {

    private String authorName;

    public Book() {

    }

    public Book(String id, String name, String authorName, int year) {
        super(id, name, year, LibraryItemCategory.BOOK);
        this.authorName = authorName;
    }

    @Override
    public String toString() {
        String result = String.format("%1$5s %2$20s %3$20s %4$10s", id, name, authorName, year);
        return result + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return id != null ? id.equals(book.id) : book.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}

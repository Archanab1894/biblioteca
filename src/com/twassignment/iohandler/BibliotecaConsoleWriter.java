package com.twassignment.iohandler;

// handles the bibliotecaConsoleWriter for the biblioteca application

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BibliotecaConsoleWriter implements BibliotecaWriteable {
    private String checkoutBookSuccessful;
    private String checkoutBookUnsuccessful;
    private String returnBookSuccessful;
    private String returnBookUnsuccessful;
    private String checkoutMovieSuccessful;
    private String checkoutMovieUnsuccessful;
    private String returnMovieSuccessful;
    private String returnMovieUnsuccessful;

    public BibliotecaConsoleWriter() {
        initializeMessagesFromPropertyFile();
    }

    private void initializeMessagesFromPropertyFile() {
        Properties properties = new Properties();
        InputStream input;

        try {
            input = new FileInputStream("resources/config.properties");
            properties.load(input);
            returnBookSuccessful = properties.getProperty("successfulBookReturn");
            returnBookUnsuccessful = properties.getProperty("unsuccessfulBookReturn");
            checkoutBookSuccessful = properties.getProperty("successfulBookCheckout");
            checkoutBookUnsuccessful = properties.getProperty("unsuccessfulBookCheckout");
            returnMovieSuccessful = properties.getProperty("successfulMovieReturn");
            returnMovieUnsuccessful = properties.getProperty("unsuccessfulMovieReturn");
            checkoutMovieSuccessful = properties.getProperty("successfulMovieCheckout");
            checkoutMovieUnsuccessful = properties.getProperty("unsuccessfulMovieCheckout");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void  displayDemarcator(){
        System.out.println("==============================================================================");
    }
    public void display(String string) {
        System.out.println(string);
    }

    public void displayListOfBooks(String string) {
        displayDemarcator();
        System.out.println(" ISBN\t\t\tName\t\t\t\tAuthor Name\t\tPublished Year\n");
        displayDemarcator();
        System.out.println(string);
        displayDemarcator();
    }

    public void displayListOfMovies(String string) {
        displayDemarcator();
        System.out.println(" Movie ID\t\t\tName\t\t\tDirector Name\tPublished Year\tRating\n");
        displayDemarcator();
        System.out.println(string);
        displayDemarcator();
    }

    public void displayBookCheckoutSuccessful() {
        System.out.println(checkoutBookSuccessful);
    }

    public void displayBookCheckoutUnsuccessful() {
        System.out.println(checkoutBookUnsuccessful);
    }

    public void displayBookReturnSuccessful() {
        System.out.println(returnBookSuccessful);
    }

    public void displayBookReturnUnsuccessful() {
        System.out.println(returnBookUnsuccessful);
    }

    public void displayMovieCheckoutSuccessful() {
        System.out.println(checkoutMovieSuccessful);
    }

    public void displayMovieCheckoutUnsuccessful() {
        System.out.println(checkoutMovieUnsuccessful);
    }

    public void displayMovieReturnSuccessful() {
        System.out.println(returnMovieSuccessful);
    }

    public void displayMovieReturnUnsuccessful() {
        System.out.println(returnMovieUnsuccessful);
    }

    public void displayCheckOuts(String result) {
        displayDemarcator();
        System.out.println(" \tItem Name\t\tUser ID\n");
        displayDemarcator();
        System.out.println(result);
        displayDemarcator();
    }

    public void displayUserInformation(String details) {
        displayDemarcator();
        System.out.println(" \tName\t\tPhone Number\t\tEmail Address");
        displayDemarcator();
        System.out.println(details);
        displayDemarcator();
    }
}

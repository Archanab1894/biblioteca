package com.twassignment.iohandler;

// handles the input for biblioteca application

import java.util.Scanner;

public class BibliotecaConsoleReader implements BibliotecaReadable {
    private Scanner in = new Scanner(System.in);
    private BibliotecaConsoleWriter bibliotecaConsoleWriter = new BibliotecaConsoleWriter();

    public String getChoiceForUserMenu() {
        String choice = "";
        do {
            bibliotecaConsoleWriter.display("\tEnter a valid option :");
            choice = in.next();
        } while (!choice.matches("[A-I]"));
        return choice;
    }

    public String getChoiceForLoginMenu() {
        String choice = "";
        do {
            bibliotecaConsoleWriter.display("\tEnter a valid option :");
            choice = in.next();
        } while (!choice.matches("[A-D]"));
        return choice;
    }

    public String getIDOfTheItem() {
        bibliotecaConsoleWriter.display("Enter the ID number:");
        String id = in.next();
        return id;
    }

    public String readCredentials() {
        String result = "";
        String pattern = "^([A-Za-z]{3})-([0-9]{4})$";

        while (!result.matches(pattern)) {
            bibliotecaConsoleWriter.display("Enter a valid user name:");
            result = in.next();
        }
        result += ",";
        bibliotecaConsoleWriter.display("Enter password:");
        result += in.next();
        return result;
    }

    public String getChoiceForLibrarianMenu() {
        String choice = "";
        do {
            bibliotecaConsoleWriter.display("\tEnter a valid option :");
            choice = in.next();
        } while (!choice.matches("[A-C]"));
        return choice;
    }

}

package com.twassignment.iohandler;


public interface BibliotecaWriteable {
    void display(String string);

    void displayListOfBooks(String string);

    void displayListOfMovies(String string);

    void displayBookCheckoutSuccessful();

    void displayBookCheckoutUnsuccessful();

    void displayBookReturnSuccessful();

    void displayBookReturnUnsuccessful();

    void displayMovieCheckoutSuccessful();

    void displayMovieCheckoutUnsuccessful();

    void displayMovieReturnSuccessful();

    void displayMovieReturnUnsuccessful();

    void displayCheckOuts(String result);

    void displayUserInformation(String details);
}

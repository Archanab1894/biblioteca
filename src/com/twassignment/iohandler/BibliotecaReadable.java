package com.twassignment.iohandler;


public interface BibliotecaReadable {
    String getChoiceForUserMenu();

    String getIDOfTheItem();

    String getChoiceForLoginMenu();

    String readCredentials();

    String getChoiceForLibrarianMenu();
}

package com.twassignment.menu;

// models menu options for Librarian

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.iohandler.BibliotecaConsoleReader;
import com.twassignment.iohandler.BibliotecaConsoleWriter;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LibrarianMenu {
    private Map<String, MenuOption> libraryOperations = new HashMap<>();
    private List<String> librarianMenu = new ArrayList<>();
    private BibliotecaReadable reader = new BibliotecaConsoleReader();
    private BibliotecaWriteable writer = new BibliotecaConsoleWriter();
    private Library library;


    public LibrarianMenu(Library library, LibraryUser currentUser) {
        this.library = library;
    }

    public void start() {
        setUpLibrarianMenu();
        createMenu();
        displayLibrarianMenu();
    }

    private void setUpLibrarianMenu() {
        libraryOperations.put("A", new ViewCheckOutsOption());
        libraryOperations.put("B", new LogoutOption());
        libraryOperations.put("C", new ExitOption());
    }

    private void createMenu() {
        librarianMenu.add("-----LIBRARIAN MENU-----");
        librarianMenu.add("A. View Checkouts");
        librarianMenu.add("B. Logout");
        librarianMenu.add("C. Exit");
    }

    private String generateMenu() {
        String menu = "";
        for (String menuItem : librarianMenu) {
            menu += menuItem + "\n";
        }
        return menu;
    }

    private void displayLibrarianMenu() {
        String option;
        do {
            writer.display(generateMenu());
            option = reader.getChoiceForLibrarianMenu();
            libraryOperations.get(option).execute(library, reader, writer);
        } while (!option.equals("B"));
    }

}

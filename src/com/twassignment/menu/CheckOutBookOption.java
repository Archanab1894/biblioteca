package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryItemCategory;
import com.twassignment.library.LibraryUser;
//enables checking out of books from library

public class CheckOutBookOption implements MenuOption {

    private LibraryUser currentUser;

    public CheckOutBookOption(LibraryUser currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        String id = reader.getIDOfTheItem();
        if (library.checkOutItem(id, LibraryItemCategory.BOOK, currentUser)) {
            writer.displayBookCheckoutSuccessful();
            return;
        }
        writer.displayBookCheckoutUnsuccessful();
    }
}

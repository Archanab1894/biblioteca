package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryItemCategory;
import com.twassignment.library.LibraryUser;

// enables returning back of movie back to  library

public class ReturnMovieOption implements MenuOption {

    private LibraryUser currentUser;

    public ReturnMovieOption(LibraryUser currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        String id = reader.getIDOfTheItem();
        if (library.returnBackTheItem(id, LibraryItemCategory.MOVIE, currentUser)) {
            writer.displayMovieReturnSuccessful();
            return;
        }
        writer.displayMovieReturnUnsuccessful();
    }
}

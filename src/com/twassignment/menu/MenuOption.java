package com.twassignment.menu;

// represents each execution path

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.*;

public interface MenuOption {

    void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer);

}

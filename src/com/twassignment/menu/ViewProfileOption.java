package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryUser;

// enables to view the user profile information

public class ViewProfileOption implements MenuOption {

    private LibraryUser currentUser;

    public ViewProfileOption(LibraryUser currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        writer.displayUserInformation(currentUser.getDetails());
    }
}

package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryItemCategory;

// enables listing of movies in the library

public class ListMoviesOption implements MenuOption {

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        writer.displayListOfMovies(library.getListOfItems(LibraryItemCategory.MOVIE));
    }
}

package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;

// enables exiting from the application

public class ExitOption implements MenuOption {

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        System.exit(0);
    }

}

package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;

//enables logging out from user account

public class LogoutOption implements MenuOption {

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        return;
    }

}

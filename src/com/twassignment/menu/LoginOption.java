package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryUser;
import com.twassignment.library.LibraryUserAccounts;

//models login of user into library management system

public class LoginOption implements MenuOption {

    private LibraryUserAccounts userAccounts = new LibraryUserAccounts();

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        String credentials = reader.readCredentials();
        String[] parameters = credentials.split(",");

        LibraryUser currentUser = userAccounts.fetchTheUserAccount(parameters[0], parameters[1]);
        if (currentUser == null) {
            writer.display("Invalid Credentials!!!");
            return;
        }
        if (currentUser.isUserALibrarian()) {
            new LibrarianMenu(library, currentUser).start();
            return;
        }
        new UserMenu(library, currentUser).start();
        return;
    }
}

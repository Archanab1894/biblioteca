package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;

//enables to view the checkouts that have been done

public class ViewCheckOutsOption implements MenuOption {

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        String result = library.viewCheckOuts();
        writer.displayCheckOuts(result);
    }
}

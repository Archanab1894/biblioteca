package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaConsoleWriter;
import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.iohandler.BibliotecaConsoleReader;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryUserAccounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// models  menu with menu options

public class LoginMenu {

    private Map<String, MenuOption> libraryOperations = new HashMap<>();
    private List<String> loginMenu = new ArrayList<>();
    private BibliotecaReadable reader = new BibliotecaConsoleReader();
    private BibliotecaWriteable writer = new BibliotecaConsoleWriter();
    private Library library;

    public LoginMenu(Library library) {
        this.library = library;
    }

    public void start() {
        setUpLoginMenu();
        createMenu();
        displayLoginMenu();
    }

    private void setUpLoginMenu() {
        libraryOperations.put("A", new ListBooksOption());
        libraryOperations.put("B", new ListMoviesOption());
        libraryOperations.put("C", new LoginOption());
        libraryOperations.put("D", new ExitOption());
    }

    private void createMenu() {
        loginMenu.add("-----MAIN MENU-----");
        loginMenu.add("A. List the books");
        loginMenu.add("B. List the movies");
        loginMenu.add("C. Login");
        loginMenu.add("D. Exit");
    }

    private String generateMenu() {
        String menu = "";
        for (String menuItem : loginMenu) {
            menu += menuItem + "\n";
        }
        return menu;
    }

    private void displayLoginMenu() {
        String option;
        do {
            writer.display(generateMenu());
            option = reader.getChoiceForLoginMenu();
            libraryOperations.get(option).execute(library, reader, writer);
        } while (!option.equals("D"));
    }

}

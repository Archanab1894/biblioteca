package com.twassignment.menu;

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryItemCategory;
import com.twassignment.library.LibraryUser;

//enables returning back of book to library

public class ReturnBookOption implements MenuOption {

    private LibraryUser currentUser;

    public ReturnBookOption(LibraryUser currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void execute(Library library, BibliotecaReadable reader, BibliotecaWriteable writer) {
        String id = reader.getIDOfTheItem();
        if (library.returnBackTheItem(id, LibraryItemCategory.BOOK, currentUser)) {
            writer.displayBookReturnSuccessful();
            return;
        }
        writer.displayBookReturnUnsuccessful();
    }
}

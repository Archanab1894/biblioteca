package com.twassignment.menu;

// models menu for users

import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.iohandler.BibliotecaConsoleReader;
import com.twassignment.iohandler.BibliotecaConsoleWriter;
import com.twassignment.library.Library;
import com.twassignment.library.LibraryUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMenu {

    private Library library;
    private LibraryUser currentUser;
    private List<String> menu;
    private Map<String, MenuOption> libraryOperations = new HashMap<>();
    private BibliotecaReadable reader = new BibliotecaConsoleReader();
    private BibliotecaWriteable writer = new BibliotecaConsoleWriter();

    public UserMenu(Library library, LibraryUser currentUser) {
        this.library = library;
        this.currentUser = currentUser;
    }

    public void start() {
        setUpUserMenu();
        createMenu();
        displayUserMenu();
    }

    private void setUpUserMenu() {
        libraryOperations.put("A", new ListBooksOption());
        libraryOperations.put("B", new ListMoviesOption());
        libraryOperations.put("C", new CheckOutBookOption(currentUser));
        libraryOperations.put("D", new ReturnBookOption(currentUser));
        libraryOperations.put("E", new CheckOutMovieOption(currentUser));
        libraryOperations.put("F", new ReturnMovieOption(currentUser));
        libraryOperations.put("G", new ViewProfileOption(currentUser));
        libraryOperations.put("H", new LogoutOption());
        libraryOperations.put("I", new ExitOption());
    }

    private void createMenu() {
        menu = new ArrayList<>();
        menu.add("-----USER MENU-----");
        menu.add("\nA. List the books");
        menu.add("B. List the movies");
        menu.add("C. Checkout a Book");
        menu.add("D. Return a Book");
        menu.add("E. Checkout a Movie");
        menu.add("F. Return a Movie");
        menu.add("G. View my Profile");
        menu.add("H. Log out");
        menu.add("I. Exit");
    }


    private String generateMenu() {
        String bibliotecaMenu = "";
        for (String menuItem : menu) {
            bibliotecaMenu += menuItem + "\n";
        }
        return bibliotecaMenu;
    }

    private void displayUserMenu() {
        String option;
        do {
            writer.display(generateMenu());
            option = reader.getChoiceForUserMenu();
            libraryOperations.get(option).execute(library, reader, writer);
        } while (!option.equals("H"));
    }

}

package com.twassignment.library;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MovieTest {
    @Test
    public void shouldReturnTrueForTheSameMovie() {
        Movie movie = new Movie("M123","Inception", "Christoper Nolan", 2009, "9");
        assertEquals(movie, movie);
    }

    @Test
    public void shouldReturnTrueForTwoEqualMovieObjects() {
        Movie movie = new Movie("M123","Inception", "Christoper Nolan", 2009, "9");
        Movie movie1 = new Movie("M123","Inception", "Christoper Nolan", 2009, "9");
        assertEquals(movie, movie1);
    }

    @Test
    public void shouldReturnTrueForSameISBN() {
        String id = "M123";
        Movie movie = new Movie("M123","Inception", "Christoper Nolan", 2009, "9");
        assertTrue(movie.isOfSameID(id));
    }

}

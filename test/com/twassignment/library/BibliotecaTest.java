package com.twassignment.library;


import com.twassignment.application.Biblioteca;
import com.twassignment.iohandler.BibliotecaConsoleReader;
import com.twassignment.iohandler.BibliotecaConsoleWriter;
import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BibliotecaTest {

    Biblioteca biblioteca;

    @Before
    public void setUp() throws Exception {
        BibliotecaReadable reader = new BibliotecaConsoleReader();
        BibliotecaWriteable writer = new BibliotecaConsoleWriter();
        biblioteca = new Biblioteca(reader,writer);
        biblioteca.setUpLibrary();
    }

    @Test
    public void shouldReturnWelcomeMessage() {
        assertEquals("Welcome to Biblioteca!!!", biblioteca.welcomeMessage());
    }

}
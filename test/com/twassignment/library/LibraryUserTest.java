package com.twassignment.library;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LibraryUserTest {

    @Test
    public void shouldBeAbleToVerifyALibraryUserCredentials(){
        LibraryUser user = new LibraryUser("Lib-1234","12345",UserType.LIBRARIAN);
        assertTrue(user.verifyCredentials("Lib-1234","12345"));
    }

    @Test
    public void shouldBeAbleToVerifyInvalidLibraryUserCredentials(){
        LibraryUser user = new LibraryUser("Lib-1234","12345",UserType.LIBRARIAN);
        assertFalse(user.verifyCredentials("Lib-1234","12245"));
    }

    @Test
    public void shouldReturnTrueForALibrarian(){
        LibraryUser user = new LibraryUser("Lib-1234","12345",UserType.LIBRARIAN);
        assertTrue(user.isUserALibrarian());
    }

    @Test
    public void shouldReturnFalseForACustomer(){
        LibraryUser user = new LibraryUser("Lib-1234","12345",UserType.CUSTOMER);
        assertFalse(user.isUserALibrarian());
    }
}

package com.twassignment.library;

import com.mongodb.MongoClient;
import com.twassignment.application.Parser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.net.UnknownHostException;

public class LibraryCheckOutsEntryTest {

    Library library;
    LibraryUser currentUser;
    Parser parser;
    LibraryCheckOutsEntry entry;
    Book book1;
    private Morphia morphia;
    private Datastore datastore;

    @Before
    public void setUp() throws Exception {
        setDatabaseConnection();
        library = new Library(datastore);
        parser = new Parser();
        initializeLibrary();
        currentUser = new LibraryUser("usr-1000", "1000", UserType.CUSTOMER);
        book1 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        entry = new LibraryCheckOutsEntry(book1,currentUser);
    }

    private void setDatabaseConnection() {
        morphia = new Morphia();
        morphia.mapPackage("/Users/barchana/biblioteca/src/com/twassignment/library");
        try {
            datastore = morphia.createDatastore(new MongoClient(), "library");
            datastore.ensureIndexes();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private void initializeLibrary() {
        parser.populateUsers();
        parser.populateLibrarians();
        parser.populateBooks();
        parser.populateMovies();
    }

    @Test
    public void shouldReturnTrueForValidCheckOutEntry(){
        library.checkOutItem("B123",LibraryItemCategory.BOOK,currentUser);
        Assert.assertTrue(entry.validateCheckOutEntry("B123",LibraryItemCategory.BOOK,currentUser,datastore));
    }

    @Test
    public void shouldReturnFalseForInValidCheckOutEntry(){
        library.checkOutItem("B123",LibraryItemCategory.BOOK,currentUser);
        Assert.assertFalse(entry.validateCheckOutEntry("B124",LibraryItemCategory.MOVIE,currentUser,datastore));
    }
}

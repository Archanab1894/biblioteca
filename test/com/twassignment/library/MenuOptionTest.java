package com.twassignment.library;

import com.twassignment.application.Parser;
import com.twassignment.database.DatabaseConnection;
import com.twassignment.iohandler.BibliotecaReadable;
import com.twassignment.iohandler.BibliotecaWriteable;
import com.twassignment.menu.*;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;

import static org.mockito.Mockito.*;

public class MenuOptionTest {
    Book book1, book2, book3;
    Movie movie1, movie2, movie3;
    BibliotecaReadable reader;
    BibliotecaWriteable writer;
    Parser parser;
    Library library;
    LibraryUser currentUser;
    private Datastore datastore;


    @Before
    public void setUp() throws Exception {
        datastore = DatabaseConnection.setDatabaseConnection();
        library = new Library(datastore);
        reader = mock(BibliotecaReadable.class);
        writer = mock(BibliotecaWriteable.class);
        parser = new Parser();
        initializeLibrary();
        initializeBookObjects();
        initializeMovieObjects();
        currentUser = new LibraryUser("usr-1000","1000",UserType.CUSTOMER);
    }

    private void initializeLibrary() {
        parser.populateBooks();
        parser.populateMovies();
        parser.populateUsers();
        parser.populateLibrarians();
    }


    private void initializeMovieObjects() {
        movie1 = new Movie("M123", "Inception", "Christoper Nolan", 2009, "9");
        movie2 = new Movie("M124", "Jurassic Park", "Steven Spielberg", 1994, "3");
        movie3 = new Movie("M125", "Titanic", "James Cameron", 1992, "Unrated");
    }

    private void initializeBookObjects() {
        book1 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        book2 = new Book("B124", "The Wings of fire", "Abdul kalam", 1994);
        book3 = new Book("B125", "Encyclopedia", "John", 2000);
    }

    @Test
    public void shouldBeAbleToDisplayBooks() {
        String testString = "" + book1 + book2 + book3;
        new ListBooksOption().execute(library, reader, writer);
        verify(writer).displayListOfBooks(testString);
    }

    @Test
    public void shouldBeAbleToListTheMovies() {
        String testString = "" + movie1 + movie2 + movie3;
        new ListMoviesOption().execute(library, reader, writer);
        verify(writer).displayListOfMovies(testString);
    }

    @Test
    public void shouldBeAbleToCheckOutAMovieSuccessful() {
        when(reader.getIDOfTheItem()).thenReturn("M123");
        new CheckOutMovieOption(currentUser).execute(library, reader, writer);
        verify(writer).displayMovieCheckoutSuccessful();
    }

    @Test
    public void shouldDisplayMessageForUnsuccessfulCheckOutOfMovie() {
        when(reader.getIDOfTheItem()).thenReturn("M128");
        new CheckOutMovieOption(currentUser).execute(library, reader, writer);
        verify(writer).displayMovieCheckoutUnsuccessful();
    }

    @Test
    public void shouldBeAbleToReturnAMovieSuccessfully() {
        when(reader.getIDOfTheItem()).thenReturn("M123");
        new CheckOutMovieOption(currentUser).execute(library, reader, writer);
        new ReturnMovieOption(currentUser).execute(library, reader, writer);
        verify(writer).displayMovieReturnSuccessful();
    }

    @Test
    public void shouldDisplayMessageForUnsuccessfulReturnOfMovie() {
        when(reader.getIDOfTheItem()).thenReturn("M189");
        new ReturnMovieOption(currentUser).execute(library, reader, writer);
        verify(writer).displayMovieReturnUnsuccessful();
    }
    @Test
    public void shouldBeAbleToCheckOutAnBookSuccessful() {
        when(reader.getIDOfTheItem()).thenReturn("B123");
        new CheckOutBookOption(currentUser).execute(library, reader, writer);
        verify(writer).displayBookCheckoutSuccessful();
    }

    @Test
    public void shouldDisplayMessageForUnsuccessfulCheckOutOfBook() {
        when(reader.getIDOfTheItem()).thenReturn("B128");
        new CheckOutBookOption(currentUser).execute(library, reader, writer);
        verify(writer).displayBookCheckoutUnsuccessful();
    }

    @Test
    public void shouldBeAbleToReturnABookSuccessfully() {
        when(reader.getIDOfTheItem()).thenReturn("B123");
        new CheckOutBookOption(currentUser).execute(library, reader, writer);
        new ReturnBookOption(currentUser).execute(library, reader, writer);
        verify(writer).displayBookReturnSuccessful();
    }

    @Test
    public void shouldDisplayMessageForUnsuccessfulReturnOfBook() {
        when(reader.getIDOfTheItem()).thenReturn("B189");
        new ReturnBookOption(currentUser).execute(library, reader, writer);
        verify(writer).displayBookReturnUnsuccessful();
    }

    @Test
    public void shouldBeAbleToViewCheckOuts(){
        when(reader.getIDOfTheItem()).thenReturn("B123");
        new CheckOutBookOption(currentUser).execute(library, reader, writer);
        new ViewCheckOutsOption().execute(library,reader,writer);
        LibraryCheckOutsEntry checkOutsEntry = new LibraryCheckOutsEntry(book1,currentUser);
        verify(writer).displayCheckOuts(checkOutsEntry.toString().concat("\n"));
    }

    @Test
    public void shouldBeAbleToViewUserProfile(){
        new ViewProfileOption(currentUser).execute(library,reader,writer);
        verify(writer).displayUserInformation(currentUser.getDetails());
    }
}

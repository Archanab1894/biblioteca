package com.twassignment.library;

import com.twassignment.application.Parser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LibraryUserAccountsTest {
    LibraryUser user1;
    LibraryUserAccounts accounts;
    Parser parser;

    @Before
    public void setUp() throws Exception {
        user1 = new LibraryUser("usr-1000","1000",UserType.CUSTOMER);
        parser = new Parser();
        parser.populateUsers();
        accounts = new LibraryUserAccounts();
    }

    @Test
    public void shouldBeAbleToReceiveUserForGivenCredentials(){
        assertEquals(user1,accounts.fetchTheUserAccount("usr-1000","1000"));
    }

    @Test
    public void shouldReturnNullForInvalidUser(){
        assertNull(accounts.fetchTheUserAccount("usr-123","3134ac"));
    }
}

package com.twassignment.library;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BookTest {
    @Test
    public void shouldBeEqualWhenComparedWithItself() {
        Book book1 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        assertEquals(book1, book1);
    }

    @Test
    public void shouldReturnTrueForSameBooks() {
        Book book1 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        Book book2 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        assertEquals(book1, book2);
    }

    @Test
    public void shouldReturnTrueForSameISBN() {
        String isbn = "B123";
        Book book1 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        assertTrue(book1.isOfSameID(isbn));
    }
}

package com.twassignment.library;

import com.twassignment.application.Parser;
import com.twassignment.database.DatabaseConnection;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;

import static com.twassignment.library.LibraryItemCategory.BOOK;
import static org.junit.Assert.*;

public class LibraryTest {
    Library library;
    Book book1, book2, book3;
    Movie movie1, movie2, movie3;
    LibraryUser currentUser;
    Parser parser;
    private Datastore datastore;

    @Before
    public void setUp() throws Exception {
        datastore = DatabaseConnection.setDatabaseConnection();
        library = new Library(datastore);
        initializeBooks();
        initializeMovies();
        parser = new Parser();
        initializeLibrary();
        currentUser = new LibraryUser("usr-1000", "1000", UserType.CUSTOMER);
    }

    private void initializeLibrary() {
        parser.populateUsers();
        parser.populateLibrarians();
        parser.populateBooks();
        parser.populateMovies();
    }

    private void initializeMovies() {
        movie1 = new Movie("M123", "Inception", "Christoper Nolan", 2009, "9");
        movie2 = new Movie("M124", "Jurassic Park", "Steven Spielberg", 1994, "3");
        movie3 = new Movie("M125", "Titanic", "James Cameron", 1992, "Unrated");
    }


    private void initializeBooks() {
        book1 = new Book("B123", "HarryPotter", "J.k.Rowling", 2000);
        book2 = new Book("B124", "The Wings of fire", "Abdul kalam", 1994);
        book3 = new Book("B125", "Encyclopedia", "John", 2000);
    }

    @Test
    public void shouldDisplayListOfBooksWithAuthorNameAndYear() {
        String testString = "" + book1 + book2 + book3;
        assertEquals(testString, library.getListOfItems(BOOK));
    }

    @Test
    public void shouldDisplayListOfMoviesWithDirectorNameAndYear() {
        String testString = "" + movie1 + movie2 + movie3;
        assertEquals(testString, library.getListOfItems(LibraryItemCategory.MOVIE));
    }

    @Test
    public void shouldReturnTrueForSuccessfulCheckOut() {
        String id = "B123";
        assertTrue(library.checkOutItem(id, BOOK, currentUser));
    }

    @Test
    public void shouldReturnFalseForUnSuccessfulCheckOut() {
        String id = "B128";
        assertFalse(library.checkOutItem(id, BOOK, currentUser));
    }

    @Test
    public void shouldBeAbleToReturnABook() {
        String id = "B123";
        library.checkOutItem(id, BOOK, currentUser);
        assertTrue(library.returnBackTheItem(id, BOOK, currentUser));
    }

    @Test
    public void shouldReturnTrueForSuccessfulReturn() {
        String id = "B123";
        library.checkOutItem(id, BOOK, currentUser);
        assertTrue(library.returnBackTheItem(id, BOOK, currentUser));
    }

    @Test
    public void shouldReturnFalseForUnSuccessfulReturn() {
        String id = "B128";
        assertFalse(library.returnBackTheItem(id, BOOK, currentUser));
    }

    @Test
    public void shouldBeAbleToViewCheckouts(){
        String id = "B123";
        library.checkOutItem(id,BOOK,currentUser);
        LibraryCheckOutsEntry checkOutsEntry = new LibraryCheckOutsEntry(book1,currentUser);
        assertEquals(checkOutsEntry.toString().concat("\n"),library.viewCheckOuts());
    }
}
